/*console.log("Hello World")

/*1. Create and initialize a trainer object with the proper methods nd properties */

			let trainer = {
				
				name: "Ash Ketchum",
				age: 10,
				pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
				friends: {
					hoen: ["May", "Max"],
					kanto: ["Brock", "Misty"],
				},
				talk: function(){
					console.log(trainer.pokemon[0]+ " I Choose You!");
				}
			}
			console.log("Result of dot notation:");
			console.log(trainer);
			console.log(trainer.name);
			console.log("Result of square bracket notation:");
			console.log(trainer.pokemon);
			console.log("Result of talk method");
			trainer.talk();



/*	
	2. Create objects out of a Pokemon constructor 

*/

				function Pokemon(name,level){
					this.name = name;
					this.level = level;
					this.health =  level*2 ;
					this.attack=  level*1;
					this.tackle = function(target){
				let targetHealth = target.health - this.attack

				console.log(this.name + ' tackled ' + target.name);
				console.log(target.name + "'s health is now reduced to " + Number(targetHealth));

				if(targetHealth <= 0){
					target.faint()
				}
			}

			this.faint = function(){
				console.log(this.name + " fainted.")
			}
		}

				let pikachu = new Pokemon("Pikachu", 12);
			
				let geodude = new Pokemon("Geodude", 8);
				
				let mewtwo = new Pokemon("Mewtwo", 100);
				

				console.log(pikachu)
				console.log(geodude)
				console.log(mewtwo)


				geodude.tackle(pikachu);
				mewtwo.tackle(geodude);
